$(document).ready(function() {

    $("#button").click(function() {
        $(".leaf").toggle(500);
        $(".down").toggle(500);
        $(".bucket").toggle(600);

    });
});

function slide() {
    let button = document.getElementById("button");
    let sw = document.getElementById("s");

    if (button.style.justifyContent == "flex-start") {
        button.style.justifyContent = "flex-end";
        button.style.background = "radial-gradient(#013B1B,var(--ncolor))";
        s.setAttribute("value", "OFF");
        s.setAttribute("color", "white");


    } else {
        button.style.justifyContent = "flex-start";
        button.style.background = "radial-gradient(var(--ncolor), var(--gcolor))";
        s.setAttribute("value", "ON");
    }

}


function clock() {
    const date = new Date();
    let left = document.getElementById("left");
    let right = document.getElementById("right");
    let s = date.getSeconds();
    left.setAttribute("value", date.toDateString());
    right.setAttribute("value", `${date.getHours() % 12 == 0 ? 12 : date.getHours() % 12}:${date.getMinutes()} ${date.getHours() >= 12 ? 'PM' : 'AM'}`);
    let st = document.getElementById("");




    for (let i = 0; i < 6; i++) {
        st = document.getElementById(`${i}`);

        if (i == 0 && s <= 10) {
            st.innerHTML = s;
            st.style.visibility = "visible";

        } else if (i == 1 && (s > 10 && s <= 20)) {
            st.innerHTML = s;
            st.style.visibility = "visible";
        } else if (i == 2 && (s > 20 && s <= 30)) {
            st.innerHTML = s;
            st.style.visibility = "visible";
        } else if (i == 3 && (s > 30 && s <= 40)) {
            st.innerHTML = s;
            st.style.visibility = "visible";
        } else if (i == 4 && (s > 40 && s <= 50)) {
            st.innerHTML = s;
            st.style.visibility = "visible";
        } else if (i == 5 && (s > 50 && s < 60)) {
            st.innerHTML = s;
            st.style.visibility = "visible";
        } else {
            st.style.visibility = "hidden";
            st.innerHTML = "";
        }

    }


}

clock();
setInterval(clock, 1000);